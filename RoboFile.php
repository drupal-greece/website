<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
  /**
   * Quickly installs Drupal to kickstart development.
   */
  public function kickstart() {
    if ($_ENV['IS_DDEV_PROJECT']) {
      $this->_exec('drush site:install --existing-config -y');
      $this->_exec('drush uli');
    }
  }

  /**
   * Create a second database in DDEV for the old Drupal 7 site.
   */
  public function created7db() {
    if ($_ENV['IS_DDEV_PROJECT']) {
      $this->_exec('mysql -uroot -proot -hdb -e "CREATE DATABASE IF NOT EXISTS d7; GRANT ALL ON d7.* TO \'db\'@\'%\';"');
    }
  }
}
