# Greek Drupal Community

Drupal 9 website

## Requirements
- [DDEV](https://ddev.readthedocs.io/en/stable/)
- Optional but recommended: follow the "mkcert" [installation notes](https://ddev.readthedocs.io/en/stable/#installation) for local SSL

_See the [this article on DDEV documentation](https://ddev.readthedocs.io/en/stable/users/extend/customization-extendibility/#extending-configyaml-with-custom-configyaml-files) for running DDEV on a different port._

## Installation

    ddev composer install
    cp .ddev/config.local.yaml.example .ddev/config.local.yaml
    ddev restart
    ddev robo kickstart

## Development

### Migration from D7 in DDEV

     ddev robo created7db
     ddev import-db --target-db=d7 --src=d7-db.sql.gz
